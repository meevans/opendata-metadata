# to be run in /eos/project/a/atlas-outreach/projects/open-data/OpenDataTuples

import pandas as pd
import glob
import os

df = pd.read_csv('file_order.csv')
generators = ["PwPy8EG","Sh_221","Sh_222","Sh_CT10","aMcAtNloPy8EG","PwPyEG_P2012","aMcAtNloHerwigppEG","MGPy8EG_A14N","Py8EG_A14NNPDF23LO"]
generator_dict = {"PwPy8EG":"Powheg Pythia8 EventGen","Sh_221":"Sherpa 2.2.1","Sh_222":"Sherpa 2.2.2","Sh_CT10":"Sherpa CT10","aMcAtNloPy8EG":"aMC@NLO Pythia8 EventGen","PwPyEG_P2012":"Powheg Pythia EventGen P2012","aMcAtNloHerwigppEG":"aMC@NLO Herwig proton-proton EventGen","MGPy8EG_A14N":"MadGraph Pythia8 EventGen A14N","Py8EG_A14NNPDF23LO":"Pythia8 EventGen A14NNPDF23 LeadingOrder"}
for index, row in df.iterrows():
    if row['ROOT file name'][0]=='m': 
        #print(row['ROOT file name'])
        path = row['ROOT file name'].split(".")[2]+"/MC"
        if '1largeRjet1lep' in path: path = path.replace('1largeRjet1lep','1fatjet1lep')
        elif '3lep' in path: path = path.replace('3lep','2lep')
        elif '4lep' in path: path = path.replace('4lep','2lep')
        elif 'exactly2lep' in path: path = path.replace('exactly2lep','2lep')
        os.chdir(path)
        string = "mc15_13TeV."+row['ROOT file name'][3:9]+"."
        for file in glob.glob(string+"*"):
            for generator in generators:
                if generator in file[18:]: 
                    print(generator_dict[generator])
                    break
        os.chdir("../../")

    #print(row['ROOT file name']+"\t"+str(n_entries))
