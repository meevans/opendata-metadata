# to be run in /eos/project/a/atlas-outreach/projects/open-data/OpenDataTuples/renamedLargeRJets

import pandas as pd
import ROOT
import csv

df = pd.read_csv('file_order.csv')

for index, row in df.iterrows():
    if row['ROOT file name'][0]=='m': string = row['ROOT file name'].split(".")[2]+"/MC/"
    else: string = row['ROOT file name'].split(".")[1]+"/Data/"
    string += row['ROOT file name']

    # read counts
    f = ROOT.TFile.Open(string,"read")
    h = f.Get("mini")
    n_entries = h.Draw("runNumber","","goff")

    #print(row['ROOT file name']+"\t"+str(n_entries))

    with open('entries.csv','a') as fd:
        writer = csv.writer(fd)
        writer.writerow([row['ROOT file name'], n_entries])
