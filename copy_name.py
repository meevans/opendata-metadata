# to be run in this cloned directory

import pandas as pd
import ROOT
import csv

df = pd.read_csv('metadata-ATLAS-opendata-samples-2020 - meta-data-samples.csv')
DSID = 0
for index, row in df.iterrows():
    if row['DSID']!=DSID:
        name = row['Human-readable name']
        DSID = row['DSID']
    print(name)
