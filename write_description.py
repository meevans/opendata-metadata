# to be run in this cloned directory

import pandas as pd
import ROOT
import csv

df = pd.read_csv('metadata-ATLAS-opendata-samples-2020 - meta-data-samples_with_human_name.csv')

for index, row in df.iterrows():
    human_readable_collection_dict = {'1lep':'exactly 1 lepton','2lep':'at least 2 leptons','3lep':'exactly 3 leptons','4lep':'at least 4 leptons','exactly2lep':'exactly 2 leptons','1largeRjet1lep':'at least 1 lepton and at least 1 large-R jet','1lep1tau':'at least 1 lepton and exactly 1 hadronic tau','GamGam':'at least 2 photons'}
    print("This is a simulated sample at 13 TeV of the "+row['Human-readable name']+" physics process with "+human_readable_collection_dict[row['Preselection Collection']]+". The MC generator is "+row['MC simulator'])
